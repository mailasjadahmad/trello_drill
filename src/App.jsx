import { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import BoardDetails from "../src/components/BoardDetails";
import "./App.css";
import Home from "./components/Home";
import Checklist from "./components/Checklist";
import { ThemeProvider, CssBaseline, createTheme } from "@mui/material";
import Checkitem from "./components/Checkitems";

function App() {
  const [count, setCount] = useState(0);

  const theme = createTheme({
    palette: {
      mode: "light",
      text: {
        secondary: "#fff",
      },
      typography: {
        fontSize: 16,
        fontFamily: `"Inter", sans-serif;`,
      },
    },
  });

  return (
    
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/board/:boardId" element={<BoardDetails />}>
          <Route path="card/:shortLink" element={<Checklist />}>
            {/* <Route path="checklist/:checklistId" element={<Checkitem />} /> */}
          </Route>
        </Route>
      </Routes>
    </ThemeProvider>
  );
}

export default App;
