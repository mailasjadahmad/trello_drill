import React, { useEffect, useState } from "react";
import Board from "./Board";
import { Box,LinearProgress,Alert } from "@mui/material";

import "./Home.css";
import axios from "axios";
import CreateBoard from "./CreateBoard";
import Header from "./Header";

function Home() {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const id = "workspacefrompostman1";
  // const orgID = import.meta.env.VITE_ORGID;
  const boardInOrgUrl = `https://api.trello.com/1/organizations/${id}/boards?key=${apiKey}&token=${token}`;
  // const [effectCounter, setEffectCounter] = useState(0);
  const [data, setBoardData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(boardInOrgUrl)
      .then((response) => {
        setBoardData(response.data);
        console.log(response.data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, []);

  function addBoardDatahandler(singleData) {
    setBoardData([...data, singleData]);
  }

  if (loading) return <Box sx={{width:"100vw", height:"100vh"}}>< LinearProgress/></Box>; 
  if (error) return
  if (error)
  return (
    <Box
      sx={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Alert severity="error" sx={{ minWidth: "300px" }}>
        {error.response.data}
      </Alert>
    </Box>
  );

  return (
    <div className="home-container">
      <Header name="workspace from postman" />
      <div className="board-container">
        {data.map((singeBoardData) => {
          return (
            <Board
              name={singeBoardData.name}
              key={singeBoardData.id}
              color={singeBoardData.prefs.backgroundColor}
              image={singeBoardData.prefs.backgroundImage}
              id={singeBoardData.shortLink? singeBoardData.shortLink:singeBoardData.id}
            />
          );
        })}
        <CreateBoard onCreateBoardData={addBoardDatahandler} />
      </div>
    </div>
  );
}

export default Home;
