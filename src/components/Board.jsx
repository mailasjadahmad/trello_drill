import React from "react";
// import "./Board.css";
import { Box } from '@mui/material'
import { useNavigate } from "react-router-dom";

function Board(props) {
    let navigate = useNavigate()
    // console.log(props.id)
    return (
      <>
        <Box
          style={{
            cursor:"pointer",
            fontSize:"1.3rem",
            width: '400px', 
            padding: '20px 20px 70px 20px',
            color:"white",
            fontWeight:"500",
            borderRadius: '12px',
            background: props.color ? props.color : `url(${props.image}) no-repeat center/cover`,
          }}
          className="board-card"
          onClick={() => navigate(`/board/${props.id}`)}
        >
          {props.name}
        </Box>
      </>
    );
  }
  

export default Board;

// export default  Home;
