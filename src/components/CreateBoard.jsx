import React, { useState } from "react";
import { Box, TextField, Button } from "@mui/material";
import axios from "axios";

function CreateBoard({ onCreateBoardData }) {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const id = "workspacefrompostman1";
  const [displayInput, setDisplayInput] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [helperText, SetHelpertext] = useState("");

  function addBoardHandler() {
    if (!inputValue) {
      SetHelpertext("please enter a name for your board");
      setDisplayInput(true);
    } else {
      SetHelpertext("");
      const createBoardUrl = `https://api.trello.com/1/boards/?name=${inputValue}&key=${apiKey}&token=${token}`;
      axios
        .post(createBoardUrl)
        .then((response) => {
          console.log(createBoardUrl);
          console.log("New board created:", response.data);
          onCreateBoardData(response.data);
        })
        .catch((error) => {
          console.error("Error creating board:", error.response.data);
        });
      setDisplayInput(false);
      setInputValue("");
    }
  }
  function displayInputHandler() {
    setDisplayInput(!displayInput);
  }

  return (
    <>
      <Box
        style={{
          cursor: "pointer",
          fontSize: "1.3rem",
          width: "350px",
          // heigth:"120px",
          padding: "20px 20px 70px 20px",
          color: "white",
          fontWeight: "500",
          borderRadius: "12px",
          backgroundColor: "grey",
          display: `${!displayInput ? "block" : "none"}`,
        }}
        className="board-card"
        key="add-board"
        onClick={displayInputHandler}
      >
        Create new board
      </Box>

      <div
        className="input-button-cont"
        style={{
          display: `${displayInput ? "flex" : "none"}`,
          gap: "20px",
          height: "50px",
        }}
      >
        <TextField
          helperText={helperText}
          id="filled-basic"
          label="Board name"
          variant="filled"
          placeholder="Enter your board name"
          sx={{ outline: "white" }}
          className="board-name-input"
          value={inputValue}
          onChange={(e) => {
            setInputValue(e.target.value);
          }}
        />
        <Button variant="outlined" size="large" onClick={addBoardHandler}>
          Create
        </Button>
      </div>
    </>
  );
}

export default CreateBoard;
