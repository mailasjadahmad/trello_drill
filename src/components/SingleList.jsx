import React, { useState, useEffect } from "react";
import Card from "./Card";
import { Button } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
// import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import axios from "axios";

function SingleList(props) {
  const [cardData, setCardData] = useState([]);
  const [cardValue, setCardValue] = useState("");
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const listId = props.listId;
  const cardInfoInaList = `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`;
  const deleteListUrl = `https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${token}&value=true`;

  useEffect(() => {
    axios
      .get(cardInfoInaList)
      .then((Response) => {
        setCardData(Response.data);
        // setLoading(false);
        // console.log(Response.data);
      })
      .catch((error) => {
        console.log(error.Response.data);
        // setError(error);
        // setLoading(false);
      });
  }, []);

  function deleteList() {
    axios
    .put(deleteListUrl)
    .then((response) => {
      if (response && response.data) {
        console.log("Resource deleted successfully:", response.data);
        props.onListChange(response.data);
      } else {
        console.error("Empty response or response.data is undefined.");
      }
    })
    .catch((error) => {
      console.error("Error deleting resource:", error.response ? error.response.data : error.message);
    });
  }

  function listUpdateHandler(deletedCardId) {
    console.log(cardData);
    const tempArr = [...cardData];
    let index = tempArr.findIndex((element) => element.id === deletedCardId);
    // console.log(index, tempArr[index]);
    if (index !== -1) {
      tempArr.splice(index, 1);
    }
    // console.log(tempArr)
    setCardData(tempArr);

    // console.log(deletedCardData);
  }

  function createCardHandler(event) {
    event.preventDefault();
    const createCardUrl = `https://api.trello.com/1/cards?idList=${listId}&name=${cardValue}&key=${apiKey}&token=${token}`;

    axios
      .post(createCardUrl)
      .then((response) => {
        console.log("card created", response.data);
        setCardData([...cardData, response.data]);
        setCardValue("");
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  }

  return (
    <div
      className="single-list"
      style={{
        margin: "10px",
        padding: "15px 15px 5px 15px  ",
        background: "#F1F2F4",
        borderRadius: "12px",
        width: "300px",
        boxShadow: "1px 2px 3px 0px rgba(0, 0, 0, 0.2)",
      }}
      key={props.listId}
    >
      <p
        className="list-name"
        style={{
          display: "flex",
          justifyContent: "space-between",
          fontSize: "1.3rem",
          fontWeight: "400",
          marginBottom: "10px",
        }}
      >
        {props.listName}
        <span onClick={deleteList}>
          <DeleteIcon style={{ color: "#ff604a", cursor: "pointer" }} />
        </span>
      </p>
      {cardData.map((singleCard) => {
        // console.log(singleCard);
        return (
          <Card
            cardInfo={singleCard.name}
            cardId={singleCard.id}
            key={singleCard.id}
            shortLink={singleCard.shortLink}
            onCardChange={listUpdateHandler}
          />
        );
      })}

      <form
        id="myForm"
        style={{ padding: "10px 0px" }}
        onSubmit={createCardHandler}
      >
        <input
          type="text"
          id="textInput"
          name="textInput"
          placeholder="Add card name"
          // defaultValue=""
          value={cardValue}
          onChange={(e) => {
            setCardValue(e.target.value);
          }}
          style={{
            border: "none",
            background: "#ffffff54",
            margin: "0px 5px 0px 0px",
            padding: "10px",
            borderRadius: "6px",
          }}
        />

        <Button type="submit" variant="contained" size="small">
          Add
        </Button>
       
      </form>
    </div>
  );
}

export default SingleList;
