import React, { useEffect, useState, useContext } from "react";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import { Button } from "@mui/material";
import {
  Box,
  LinearProgress,
  Modal,
  Checkbox,
  FormControlLabel,
} from "@mui/material";
import axios from "axios";
// import { useNavigate, useParams } from "react-router-dom";
// import CheckitemContext from "../context/CheckitemContext";

function Checkitem({ checklistId, shortLink, onChangeStatus }) {
  const [checkitemData, setCheckitemtData] = useState([]);
  const [itemValue, setItemValue] = useState("");
  const [checkitemStatusData, setCheckitemStatusData] = useState({});
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    const checkitemsDataUrl = ` https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${apiKey}&token=${token}`;
    axios
      .get(checkitemsDataUrl)
      .then((response) => {
        setCheckitemtData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching checklist data:", error);
      });
  }, []);

  const checkBoxChange = async (singleCheckItem) => {
    const newState =
      singleCheckItem.state == "complete" ? "incomplete" : "complete";
    const tempData = [...checkitemData];
    const index = tempData.findIndex((each) => each.id == singleCheckItem.id);

    if (index != -1) {
      tempData[index].state = newState;
      setCheckitemtData(tempData);
    }
    console.log(tempData[index]);

    const checkitemId = tempData[index].id;
    const state = tempData[index].state;
    setCheckitemStatusData({ state, checklistId });

    onChangeStatus(tempData[index]);

    const updateCheckUrl = `https://api.trello.com/1/cards/${shortLink}/checkItem/${checkitemId}?&state=${state}&key=${apiKey}&token=${token}`;
    axios
      .put(updateCheckUrl)
      .then((response) => {
        console.log("Update successful:", response.data);
      })
      .catch((error) => {
        console.error("Response data:", error.response.data);
      });
  };

  function deleteCheckitemHandler(checkitem) {
    const checkitemId = checkitem.id;
    const deletedCheckitemUrl = ` https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkitemId}?key=${apiKey}&token=${token}`;

    axios.delete(deletedCheckitemUrl).then((response) => {
      const tempArr = [...checkitemData];
      let index = tempArr.findIndex((element) => element.id === checkitemId);
      if (index !== -1) {
        tempArr.splice(index, 1);
      }
      setCheckitemtData(tempArr);
    });
    onChangeStatus({"checklistId":checklistId,"action":"minus", state:checkitem.state})
  }

  function createCheckitemHandler(event) {
    event.preventDefault();
    const checkitemsDataUrl = ` https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${itemValue}&key=${apiKey}&token=${token}`;
    axios
      .post(checkitemsDataUrl)
      .then((response) => {
        setCheckitemtData([...checkitemData, response.data]);
        setItemValue("");
      })
      .catch((error) => {
        console.log(error.response.data);
      });
      onChangeStatus({"checklistId":checklistId,"action":"add"})
  }

  return (
    <div style={{ background: "#1565c030", padding: "20px" }}>
      {checkitemData.length > 0 ? (
        ""
      ) : (
        <p style={{ color: "#1565c0" }}> Add a new checkitem</p>
      )}
      <div
        className="checkboxex-container"
        style={{
          height:"fit-content",
          display: "flex",
          gap: "20px",
          flexDirection: "column",
        }}
      >
        {checkitemData.map((singleCheckItem) => (
          <div
            key={singleCheckItem.id}
            itemID={singleCheckItem.id}
            style={{
              cursor: "pointer",
              display: "flex",
              overflowWrap: "break-word",
              justifyContent: "space-between",
              gap: "10px",
              border: "solid 1px rgb(44 154 108 / 25%)",
              padding: "10px",
              borderRadius: "9px",
            }}
          >
            <span style={{ display: "flex", gap: "0px", width: "90%" }}>
              <FormControlLabel
                key={singleCheckItem.id}
                control={
                  <Checkbox
                    checked={singleCheckItem.state == "complete"}
                    onChange={() => checkBoxChange(singleCheckItem)}
                    style={{ color: "#1565c0", height: "20px", margin: "0px" }}
                  />
                }
              />
              <span style={{ overflowWrap: "break-word", width: "90%" }}>
                {singleCheckItem.name}
              </span>
            </span>

            <DeleteOutlineIcon
              onClick={() => {
                deleteCheckitemHandler(singleCheckItem);
              }}
              style={{ color: "#ff604a", cursor: "pointer" }}
            />
          </div>
        ))}
      </div>
      <form
        id="myForm"
        style={{ padding: "10px 0px", width: "100%" }}
        onSubmit={createCheckitemHandler}
      >
        <input
          type="text"
          id="textInput"
          name="textInput"
          placeholder="Add checkitem name"
          value={itemValue}
          onChange={(e) => {
            // console.log(itemValue)
            setItemValue(e.target.value);
          }}
          style={{
            width: "290px",
            border: "none",
            background: "#8080801f",
            margin: "15px 5px 0px 0px",
            padding: "12px",
            borderRadius: "6px",
          }}
        />
        <Button type="submit" variant="contained" size="medium">
          Add
        </Button>
      </form>
    </div>
  );
}

export default Checkitem;
