import React from "react";

function Header(props){
return( <div className="home-header" style={{width:"full-width", boxShadow: "1px 2px 3px 0px rgba(5, 5, 0, 0.2)",}}>{props.name}</div>)
}

export default Header;