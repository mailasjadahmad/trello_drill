import React, { useEffect, useState } from "react";
import Header from "./Header";
import { Alert, Box, LinearProgress } from "@mui/material";
import { Outlet, useParams } from "react-router-dom";
import { Button } from "@mui/material";
import axios from "axios";
import SingleList from "./SingleList";

function BoardDetails() {
  let { boardId } = useParams();
  const [listData, setListData] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [boardName, setBoardName] = useState("");
  const [displayInput, setDisplayInput] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  const listInBoardUrl = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${token}`;
  const boardInfoUrl = `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${token}`;
  const addListUrl = `https://api.trello.com/1/boards/${boardId}/lists?name=${inputValue}&key=${apiKey}&token=${token}`;

  useEffect(() => {
    Promise.all([axios.get(listInBoardUrl), axios.get(boardInfoUrl)])
      .then(([listResponse, boardResponse]) => {
        setListData(listResponse.data);
        setBoardName(boardResponse.data.name);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, []);

  function handleAddList(event) {
    event.preventDefault();
    axios
      .post(addListUrl)
      .then((response) => {
        console.log("New list created:", response.data);
        setListData([...listData, response.data]);
      })
      .catch((error) => {
        console.error("Error creating board:", error.response.data);
        console.log(addListUrl, inputValue);
      });

    setDisplayInput(false);
    setInputValue("");
  }

  function listChangeHandler(deletedListData) {
    const tempArr = [...listData];
    let index = tempArr.findIndex(
      (element) => element.id === deletedListData.id
    );
    if (index !== -1) {
      tempArr.splice(index, 1);
    }

    setListData(tempArr);
  }

  if (loading)
    return (
      <Box sx={{ width: "100vw", height: "100vh" }}>
        <LinearProgress />
      </Box>
    );
  if (error)
    return (
      <Box
        sx={{
          width: "100vw",
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Alert severity="error" sx={{ minWidth: "300px" }}>
          {error.response.data}
        </Alert>
      </Box>
    );
  return (
    <div className="single-board-page" style={{ minWidth: "100vw" }}>
      <Header name={boardName} />
      <div
        className="list-containre"
        style={{
          padding: "10px 30px",
          display: "flex",
          alignItems: "flex-start",
        }}
      >
        {listData.map((singleList) => {
          return (
            <SingleList
              listName={singleList.name}
              listId={singleList.id}
              key={singleList.id}
              onListChange={listChangeHandler}
            />
          );
        })}

        <div
          className="add-list"
          style={{
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
            margin: "10px",
            padding: "10px",
            background: "#1B85C5",
            borderRadius: "12px",
            color: "white",
            minWidth: "250px",
            boxShadow: "1px 2px 3px 0px rgba(0, 0, 0, 0.2)",
          }}
        >
          <form
            id="myForm"
            style={{ padding: "10px 0px", display: "flex" }}
            onSubmit={handleAddList}
          >
            <input
              type="text"
              id="textInput"
              name="textInput"
              placeholder="Add list name"
              // defaultValue=""
              value={inputValue}
              onChange={(e) => {
                setInputValue(e.target.value);
              }}
              style={{
                border: "none",
                background: "#ffffff70",
                margin: "0px 5px 0px 0px",
                padding: "10px",
                borderRadius: "6px",
              }}
            />

            <Button
              type="submit"
              variant="text"
              size="small"
              sx={{ color: "white" }}
            >
              Add
            </Button>
          </form>
        </div>
      </div>
      <Outlet />
    </div>
  );
}

export default BoardDetails;
