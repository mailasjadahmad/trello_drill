import { Box, Modal } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { styled } from "@mui/material/styles";
import DeleteIcon from "@mui/icons-material/Delete";
import { Button } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import Checkitem from "./Checkitems";

function Checklist() {
  let { shortLink } = useParams();
  const navigate = useNavigate();
  const [checklistData, setChecklistData] = useState([]);
  const [cardName, setCardName] = useState("");
  const [listValue, setListValue] = useState("");
  const [loading, setLoading] = useState(true);
  const [open, setOpen] = useState(true); 

  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    const checklistsDataUrl = `https://api.trello.com/1/cards/${shortLink}/checklists?name=${listValue}&key=${apiKey}&token=${token}`;
    const cardDetailofCheclist = `https://api.trello.com/1/cards/${shortLink}?key=${apiKey}&token=${token}`;
    const promises = [
      axios.get(checklistsDataUrl),
      axios.get(cardDetailofCheclist),
    ];

    Promise.all(promises)
      .then((responses) => {
        const [checklistsResponse, cardDetailsResponse] = responses;
        const Data = checklistsResponse.data;
        const cardDetails = cardDetailsResponse.data;
        checklistDatamodifier(Data);
        setCardName(cardDetails.name);
      })
      .catch((errors) => {
        console.error("Error fetching data:", errors);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 5,
    width: "100%",
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
    },
  }));


  function checklistDatamodifier(Data) {
    Data.map((singleChecklist) => {
      let completed = 0,
        remaining = 0;
      singleChecklist.checkItems.forEach((singleCheckitem) => {
        if (singleCheckitem.state)
          singleCheckitem?.state === "complete" ? completed++ : remaining++;
      });
      singleChecklist["completedItem"] = completed;
      singleChecklist["remainingItem"] = remaining;
      
      singleChecklist["progress"]=(completed)/(completed+remaining);
      return singleChecklist;
    });
    setChecklistData(Data);
  }

  function handleChangedStatus(data) {
    const tempArr = [...checklistData];
    let index = tempArr.findIndex((element) => element.id === data.checklistId);
    
    if (data.checklistId) {
      if (data.action === "add") {
        tempArr[index].remainingItem++;
      } else if (data.action === "minus") {
        if (data.state === "incomplete") {
          tempArr[index].remainingItem--;
        } else {
          tempArr[index].completedItem--;
        }
      }
      tempArr[index].progress=(tempArr[index].completedItem)/(tempArr[index].completedItem+tempArr[index].remainingItem);
    } else {
      const status = data.state;
      let index = tempArr.findIndex((element) => element.id === data.idChecklist);
      if (status === "complete") {
        tempArr[index].remainingItem--;
        tempArr[index].completedItem++;
      } else {
        tempArr[index].remainingItem++;
        tempArr[index].completedItem--;
      }
      tempArr[index].progress=(tempArr[index].completedItem)/(tempArr[index].completedItem+tempArr[index].remainingItem);
    }
    
    setChecklistData(tempArr);
  }
  

  function deleteChecklistHandler(checklist) {
    const cardId = checklist.idCard;
    const checklistId = checklist.id;
    const tempArr = checklistData;

    const deletedChecklistUrl = `https://api.trello.com/1/cards/${cardId}/checklists/${checklistId}?key=${apiKey}&token=${token}`;
    axios.delete(deletedChecklistUrl).then((response) => {
      setChecklistData(response.data);
      checklistDatamodifier(response.data);
    });
  }

  function createListHandler(event) {
    event.preventDefault();
    const checklistsDataUrl = `https://api.trello.com/1/cards/${shortLink}/checklists?name=${listValue}&key=${apiKey}&token=${token}`;
    axios
      .post(checklistsDataUrl)
      .then((response) => {
        checklistDatamodifier([...checklistData, response.data]);
        setListValue("");
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  }

 function handleClose(){
  navigate(-1);
  setOpen(false)
 }

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="parent-modal-title"
      aria-describedby="parent-modal-description"
      style={{minHeight:"100vh"}}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 600,
            bgcolor: "background.paper",
            borderRadius: "0.5rem",
            boxShadow: 24,
            padding: "20px",
          }}
        >
          {loading ? (
            <LinearProgress />
          ) : (
            <div style={{maxHeight:"80vh",overflow:"scroll"}}>
              <div
                className="checkboxex-container"
                style={{
                  display: "flex",
                  gap: "20px",
                  flexDirection: "column",
                }}
              >
                <p
                  className="checklist-name"
                  style={{
                    fontSize: "2rem",
                    fontWeight: "600",
                    color: "#1565c0",
                  }}
                >
                  {checklistData.length > 0 ? (
                    cardName
                  ) : (
                    <p style={{ fontSize: "1rem" }}>Add a new checklist</p>
                  )}
                </p>
                {checklistData.map((singleChecklist) => (
                  <div>
                    <div
                      key={singleChecklist.id}
                      listid={singleChecklist.id}
                      style={{
                        cursor: "pointer",
                        display: "flex",
                        overflowWrap: "break-word",
                        justifyContent: "space-between",
                        gap: "10px",
                        background: "#1565c030",
                        padding: "10px",
                        borderRadius: "9px 9px 0px 0px",
                      }}
                    >
                      <span
                        style={{ display: "flex", gap: "10px", width: "90%" }}
                      >
                        <CheckBoxIcon
                          style={{
                            height:"31px",
                            color: `${
                              singleChecklist.remainingItem == 0 &&
                              singleChecklist.completedItem > 0
                                ? "#1565c0"
                                : "#80808059"
                            }`,
                          }}
                        />

                        <span
                          style={{ fontSize:"1.3rem",fontWeight:"600",overflowWrap: "break-word", width: "80%" }}
                        >
                          {singleChecklist.name}
                        </span>
                      </span>
                      {!isNaN(singleChecklist.completedItem) &&
                      !isNaN(singleChecklist.remainingItem) ? (
                        <p style={{ color: "#1565c0" }}>
                          {singleChecklist.completedItem}/
                          {singleChecklist.remainingItem +
                            singleChecklist.completedItem}
                        </p>
                      ) : (
                        <p style={{ color: "#1565c0" }}>0/0</p>
                      )}

                      <DeleteIcon
                        onClick={() => {
                          deleteChecklistHandler(singleChecklist);
                        }}
                        style={{ color: "#ff604a", cursor: "pointer" }}
                      />
                    </div>
                    <Checkitem
                      checklistId={singleChecklist.id}
                      shortLink={singleChecklist.idCard}
                      onChangeStatus={handleChangedStatus}
                    />
                    <BorderLinearProgress variant="determinate" value={singleChecklist.progress? (singleChecklist.progress * 100):(0)} />
                  
                  </div>
                ))}
              </div>
              <form
                id="myForm"
                style={{ padding: "10px 0px", width: "100%" }}
                onSubmit={createListHandler}
              >
                <input
                  type="text"
                  id="textInput"
                  name="textInput"
                  placeholder="Add checklist name"
                  value={listValue}
                  onChange={(e) => {
                    setListValue(e.target.value);
                  }}
                  style={{
                    width: "290px",
                    border: "none",
                    background: "#8080801f",
                    margin: "15px 5px 0px 0px",
                    padding: "12px",
                    borderRadius: "6px",
                  }}
                />
                <Button type="submit" variant="contained" size="medium">
                  Add
                </Button>
              </form>
            </div>
          )}
        </Box>
      </Box>
    </Modal>
  );
}

export default Checklist;
