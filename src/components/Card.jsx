import React, { useState, useEffect } from "react";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function Card({ cardInfo, cardId, shortLink, onCardChange }) {
  const navigate = useNavigate();
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const delCardUrl = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`;

  function delCard() {
    onCardChange(cardId);
    axios
      .delete(delCardUrl)
      .then((response) => {
        console.log("Resource deleted successfully:", response.data);
      })
      .catch((error) => {
        console.error("Error deleting resource:", error.response.data);
      });
  }

  return (
    <div
      className="single-card"
      style={{
        display: "flex",
        gap: "5px",
        justifyContent: "space-between",
        alignItems: "flex-start",
        background: "#fff",
        boxShadow: "1px 2px 3px 0px rgba(0, 0, 0, 0.1)",
        padding: "6px 10px",
        borderRadius: "9px",
        overflowWrap: "break-word",
        margin: "10px 0px",
      }}
    >
      <p
        className="card-info"
        style={{ width: "90%", cursor: "pointer" }}
        onClick={() => navigate(`card/${shortLink}`)}
      >
        {cardInfo}
      </p>
      <div onClick={delCard} style={{ cursor: "pointer" }}>
        <DeleteOutlineIcon style={{ color: "#ff604a" }} />
      </div>
    </div>
  );
}

export default Card;
