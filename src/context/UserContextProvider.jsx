import React, { useState } from "react";
import CheckitemContext from "./CheckitemContext";

const CheckitemContextProvider = ({ children }) => {
  const [checkBoxupdate, SetCheckBoxupdate] = useState("hello");
  // console.log(checkBoxupdate)
  return (
    <CheckitemContext.Provider value={{ checkBoxupdate, SetCheckBoxupdate }}>
      {children}
    </CheckitemContext.Provider>
  );
};

export default CheckitemContextProvider;
