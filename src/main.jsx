import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import {
  BrowserRouter,
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "./index.css";
import CheckitemContextProvider from "./context/UserContextProvider.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(


  <BrowserRouter>
    {/* <CheckitemContextProvider> */}
      <App />
    {/* </CheckitemContextProvider> */}
  </BrowserRouter>
);
